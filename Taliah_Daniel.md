
# 2021-09-16 #

* The professor provided a video and my take on it was: a fully electric economy can change our everyday lives as items will be cheaper.
* I think that with automation there will be abundance.
* In addition, exponential growth leads to abundance, especially in the technological areas.

# 2021-09-23 #

* Based on the TED talk 'Is the world getting better or worse' I can tell that the world is getting much better.
* I really enjoy this course material because it kind of deviates from that of normal classes, because it teaches us about the real world and prepares us for the professional world.
* Confused about GIT.

# 2021-09-30 #

* I finally understood why we use GIT. It is because we are moving in a direction where automation is done in code. In most professions it is important to learn and understand code. GIT helps with that.
* I love the fact that we watch TED talks in every class because they are my favorite videos to watch.
* My take from the first video was: Alot of data visualizations will overstate uncertainty i.e when a statistic is heard people become skeptical, but as soon as it is buried in a chart it seems like some objective science.
* The second video which was entitled ' How to seek truth in the era of fake news' did not give viable pointers on how to fulfill that purpose. Three things that the presenter said was: 1. Be careful where you get your information from. 2. Take responsibility for what you read, listen to and watch. 3. Make sure that you got to the trusted brands toget you information. But my question is how to you even know that the information is real?
* Homework points: I read the news on Japan planning to release contaminated water from Fukishima into the sea, from three different sources and all three of them were quite accurate in the information they provided.
* Source 1 was from Europe on theguardian.com and the material published is as follows: "Japan announces that iT will release more than 1m tonnes of contaminated water from the wrecked fukushima nuclear power plant into the sea. Confirmation of the move which came more than a decade after the nuclear disaster. The prime minister said that releasing the water into the ocean is the most realistic option.
* Source 2 was from The Middle East on aljazeera.com and the material published is as follows: Japan says it will release more than one million tonnes of contaminated water from the ruined fukushima nuclear power station back into the sea. The work to release the water will begin in about 2 years and is expected to take decades.
* Source 3 was from Asia on chanelnewsasia.com and the material published is as follows: Japan plans to realsease into the sea more than a million tonnes of radioactive water from the destroyed Fukushima nuclear station. The work to rellease the water will begin in about two years, and the whole process is expected to take decades.

# 2021-10-07 #

* There is alot about money that I do not know, thankfully this class has taught me
* I think that the way the cenral bank ensures a stable value of their currency is bu using tools like interest rates to adjust the supply of money. That way too much or too little money isnt supplied.
* It is good to know that the world is getting richer because there are less poor people now that before.
* Even though the world is "richer" the rich still get richer and everyone else gets relatively poorer.
* The poorest people accumulate more debt than savings which is why they are poor. However they help the world get richer because they take more loans, which may be a liability for them but an asset for the bank.

# 2021-10-14 #

* Today we watched three Ted talks which is amazing.
* The first video spoke about facism. I believe that nationalism is better than facism because facism creates this fake reality. As the presenter in the video said; facism makes us see our self far more beautiful and important than we really are.
* I personally think a facist nation is a corrupt one.
* The second video was about a once white supremecist whose life changed. My take on this video is that compassion and empathy from others changes people. Also, people fear and hate what they do not understand.
* My take on the third video is that we are slowly straying away from the real world and gravitating toward a mythical one.

# 2021-10-21 #

* I find that living a healthy life is something that most of the world's population struggles with.
* I think that being healthy not only refers to the body, but also the mind, spirit and soul. As a spiritual person I live by this.
* It is so hard to live healthily in this mordern world as most foods are processed, pesticides are used on farm crops and most of the utensils that we use are made of plastic.