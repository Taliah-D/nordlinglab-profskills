* This diary file is written by YuChen_Hou E14081185

# 2021-09-24 #

* I still feel a little out of the loop in our second class.
* I'll try my best to keep up with my classmates.
* I finally contact with other member of our group.
* I used to hear of GIT, but I actually didn't use it. It's a good chance to learn about it.

# 2021-10-02 #

* A member of our group had dropped before our group meeting started, which means I lack a partner who can use the same language to collaborate. Fortunately, there is the help of Google Translate, I will try my best not to hold our group back.

* My partner is a nice person, we have successfully booked a fixed group work time after this class.

* The third class is about fake news and some details about citing statics we search on Internet.

* Unfortunately, the 3rd URL (https://newsvoice.com/) in the task after this class is broken, so that my partner and me can't answer it.