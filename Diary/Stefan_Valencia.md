﻿Diary by Stefan Valencia 王冠興 F04078423 

##  180913

  What will our world look like?

 * Many new technologies will change our lives during the comming decades
 * The combination of these new emerging technologies can bring new unforseen invetions and transformation to society
 * Exponential growth is easily overlooked especially in the beginning:
		1. In the beginning (slow growth) it can be confused or aproximated with linear growth
		2. This leads to a surprise event and disruption, if the linear projections are not updated.
		3. It is very hard for the human mind to imagine exponential growth, so we often come back to linear projections
			of the future.

Artificial Intelligence, 3D Printing, High-Level Automation, Energy shift to sustainable production and consumption, Blockchain, connecting the remaining 
3 Billion people to the internet and many more technologies will affect our lives substatially. The combination of these will result in a great variety of 
new technologies.


##  180920
  

  What does our world look like?
  
 * Even students of higher education do not have an acurate picture of the world
 * Reasons could be misrepresentation by the media:
	1. constant exageration for clickbait titles distort reality
	2. when comparing the present to the past, the daily media blows up small problems into big ones
	   and therefore distracts
	3. media does not put things into perspective (good news are almost never reported)


##  180927
  
  How to check facts and give sources?

 * One should always use apropriate quotation and supply sources to avoid getting kicked out of NCKU for plagiarism
 * Just because a statistic looks professional and nice presented does not mean that the data is representative or accurate 
 * To check data for Accuracy:
 
   1. How was the study conducted (size, questionaire, subset, who, when, where, how)
   2. Who did the study? (Could they have any bias? Do they have sponsors with interests?)
   3. Is the subset representative for the bigger picture?
   
 * Fake news can be detected by:
 
   1. overly emotional or dramatic (indicator)
   2. Checking names, dates, pictures with other data or reports
   3. Check with your own background knowledge
   4. Who reported the news? Could they be biased, have special interests? Who is paying the Journalists? 
   

##  181004
  
  How does the financial system work?

  * Traditional concept of banks that is taught in schools: Banks store money.
  * Reality: Banks lend more money than they take from deposits, therefor creating money
  * Creating money is sutainable as long as it is allocated to new value-creating businesses 
  * Creating new money for consumption is unsustainable and leads to inflation
  * The economy goes in cycles of depressions and booms:
    1. small cycles of 5 to 8 years overlapping
    2. bigger cycles of around 100 years which overlap
    3. continuos emprovement in productivity (linear growth)
  * The amount of money is small in comparison to the amount of credit
  
  
  Homework from last week:'
  
  Article: https://second-opinion.se/felaktigt-om-karnkraftens-klimatpaverkan/
  In this article, Carl Berglöf, says that the numbers and facts often quoted during political discussion
  on the future of nuclear power are either inacurate or false.
  I fact checked his "corrected" numbers:

  1. Carl Berglöf says that the typical construction time of nuclear power plants is not so long:
      only 5.5 years for the latest 4 plants (worldwide) last year.

      According to the World Nuclear Assosiation this is true:
      http://www.world-nuclear.org/gallery/world-nuclear-performance-report-gallery/construction-times-of-new-units-connected-to-the-g.aspx
      http://www.world-nuclear.org/getmedia/42c5c89b-b0d2-42da-8eb4-0dc7fa9e8124/figure-12-construction-times-of-new-units-hi-res-2018.png.aspx?width=3025&height=1008&ext=.png
      
  2. While citing numbers of CO2 per produced kWh of electricity for nuclear powerplants, Carl Berglöf compares Mr Vattenfall's Life cycle analyzes to 
     UN Climate panel IPCC reports, saying that the reports supports Vattenfall´s analyses.

     Unfortunately, the source quoted is difficult to obtain (only on paper, not online, and through purchase):
     http://www.ipcc.ch/publications_and_data/publications_and_data.shtml
     
 Conclusion: Data is often not publicly available or hard to access. 
 

## 181011
  
   Why does extremism flourish?
   
  * People often assosiate their world views and belief with their persona.
  * If their believes or views/opinions are attacked, they feel personally threatend.
  * People tend to stick to their Opinion even when presented with evidence proofing they are wrong.
  * A sense of belonging in this big world can be difficult to find, extremist groups use that as temptation.
  * The world order is big and complex, making it easier to find data (via selection bias) supporting once extremist views.
  
 
##  181018
  
  How to live healthy?
  
  * Only a healthy engineer can be a good engineer
  * How much sport do I need to stay healthy? 
  * Longer attention span through daily work outs
  * In a sick-care system doctors get paid when the need to cure the sick
  * In an ideal health-care system doctors would get paid as long as people are healthy
  * My team is going to measure the effects of a daily work out on my focus while studying:
  
    As a baseline we are going to take the average intervalls of when I loose focus while reading new material from last summer.
    At that time I was not doing to much sport, and I recorded all my study activity with an aplication precisely.
    And then we are going to compare it to next 7 days where I am going to work out daily
  

##  181025
  
  How to handle depression?
  
  * Talking to a friend with depression does not have to be a sad conversation. Normal activites are very much welcomed
  * Homework: How would a person react to a suicidal friend?
  * law inforcement agencies are not always trained for suicidal suspects
  * Depressed people often feel like they have no right to feel depressed, because their circumstances are "not tragic"
  * Anybody can be affected by depression
  * Claim for next week: Check if number of suicidal incidents on the Golden Gate Brigde since 1934 is correct
  

##  181101
  
  How to choose happiness/fulfilment?
  
  * Taking a depressed friend on vacation or a shared activity
  * Treat them as a normal friend, do normal activities that you would do with non-depressed friends as well
  * If a friend comes to you, to talk about his depressed state, then (s)he is trusting you, don´t judge them, it may harm the trust
  * When listening to a friend, try to just listen, instead of offering solutions that they probably already thought of them selfs
  
  * People often explain their actions AFTER they did them. 
  * Basically the conscious part tries to make sense of what the unconscious part of us decided
  

##  181108
  
  How does the legal system work?
  
  
  * There is a difference of legal and lawful. 
  * Lawful means it is in line with the law of the land. (Don´t be deceiving in business, don´t harm anybody or anything, don´t steal)
  * Legal means that two or more parties have an agreement. For this every party neeeds to consent.
  * E.g. not wearing a seat belt in a car: illegal but not unlawful. Not harming anybody?
  * Smart homes can send many data points which don´t necesarly violate your privacy if looked at each individual data point.
    It is mass of many data points from different devices that can make your life transparent.
  * Patents have to be scaled individually for different industries. It does not make sense to have the same rules for the software sector (fast development)
    as for the medical sector (slow development)
  
  
##  181115
  
  How to stay connected and free?
  
  * Smart phones have united many functions into one device, making them very convenient to use at any time, for any task
  * Because smart phones are on as 24/7 they can lead to several addictions such as social media addictions
  * When constantly used, they can deter social engagement and send off wrong signals
  * Is social media making us antishocial?
  
##  181122
  
  How to make sense of events?
  
  * Non-biased news does not exist.
  * Therefore it is important to evaluate the news from different perspectives
  * Countries have different press freedom indeces
  * Fake news cause huge damage to the masses who don´t read much passed the headlines
  
  
##  181129
  
  Planetary boundaries
  
  * The planetary boundaries are defined by a control value wich determines how far we can operate in a save zone
  * There are 8 planetary boundaries meaning that we can mess up our planet in 8 different ways
  * The exact boundary is often unknown, so we assign a savety margin to that threshold.
  * Some of the planetary boundaries have not yet been crossed and others are already in high risk of creating unreparable damage

##  181206
  
  Why a third industrial revolution?
 
  * A third industrial revolution is needed to make use of the new technological breakthroughs:
   * 1) New energy: Independance from Oil (unsustainable after the next century) with renewable energy such aus solar, wind, geothermal
   * 2) New communication: Internet of things let us produce the exact quantity needed by the end consumer, eliminating waste, increasing efficiency
   * 3) New transportation: From ownership of vehicles to a sharing economy, reducing cars on the road, making travel more economical and available

  * Every industrial revolution in the past came form changes in new energy (wood to coal to oil, animal power to steam to internal combustion),
    new communication (letters on paper, to telegraphs and radio, to telephonese and internet) and new transportation (hoarse to car).
  

##  181213
  
  How to succeed?
  
  * Innovation comes from trial and error, so we should be more agile and implement ideas right away.
  * Our paths are predictible: circle of friends, walk to work or school, etc. Break the routine to be exposed to new people and experiences.
  * A safe environment at work is key for the employees to feel engaged. If there are trust issues, or unfair compensation, moral will lower.
  * A sense of belonging to the company can only exist if the company also protects it´s employees especially when they are underperforming, having a difficult time etc.
  * If the employer takes the risk of trusting employees on their new ideas, the employees will feel valued and accepted and therefore enjoy work more.
  

  Report of every day productiveness over a week:
 
  2018-12-31: Mon
  A: Succesful and unproductive
  B: I got up early for a day trip to Taipei for fun. I came back with the night bus after watching the fireworks.
  C: Get some Study time tomorrow
  
  2019-01-01: Tue
  A: Succesful and productive
  B:
  * Got up late after the new years celebration.
  * Reviewed the last maths class for 30 minutes.
  * Reviewed the last Chinese lesson for 30 minutes.
  * Reviewed the missed lectures for one and a half hours.
  * Met up with a friend to watch a movie at night.
  C: Keep it up for tomorrow too!
  

  2019-01-02: Wed
  A: Succesful and productive
  B:
  * Reviewed missed lessons for 3 hours.
  * Studied Chinese 1,5 hours.
  * Collected information on applying for Masters in Taiwan.
  C: Tomorrow I really need to study for the exams in Germany, in february.

  
  2019-01-03: Thu
  A: Succesful and unproductive
  B: 
  * I got the approval for my recommendation letter from Prof. Nordling for applying Masters in Taiwan.
  * I didn´t study for my exams in Germany today  
  * Studied Chinese for 1,5 hours
  C: Really need to study tomorrow for my exams in Germany! And also make a study plan for the last 2 months before the exam.
  
  2019-01-04: Fri
  A: Succesful and productive
  B: 
  * My classmates and I interviewed some people for our assignment and hat lunch together. 
  * Later I studied for exams and for chinese.
  C:Make a rough schedule of activities in the morning.
  
  2019-01-05: Sat
  A: Succesful and unproductive
  B:
   * I got up late and went swimming on the beach with the other exchange students, since it our last few days in Taiwan. 
   * Studied some Chinese.
  C: Instead of making the schedule for the day in the morning, make it in the evening before the next day.
  
  2019-01-06: Sun
  A: Unsuccesful and unproductive
  B: 
  * Had to say Good-bye to a good friend for a long time.
  * Did not feel motivated to study at all.
  * Made a schedule for monday on what to do.
  C: Follow the schedule tomorrow.
  
  2019-01-07: Mon
  A: Succesful and productive
  B:
  * Contacted with a potential supervisor for my bachelor thesis in Munich (starting when I come back).
  * Bachelor Thesis looks promosing. I feel better now that things are more organized. 
  * Studied for exams and Chinese.
  * Going to Good-bye party tonight.
  * wrote the schedule for tomorrow.
  C: Set lower goals for the daily schedule, so that work does not seem overwhelming and demotivating.
  
  
##  181220
  
  How to solve our problems?
  
  * Difficult situations let people come up with creative solutions to problems.
  * Open sourcing of ideas and invention for the benefit of all, instead of patent wars.
  * Similar to how electricity transformed industries, AI can be added to any business as a tool to improve it or provide new services.
  * A sense of community can help to solve problems: i.e. picking up and categorizing litter with Litterati.
  

##  181227
  
  How to make a difference?
  
  * In order to make a massage viral on social media, it helps to create a sense of community (even for a short time).
  * People are often remembered for helping others after they passed away
  * To discover the marvels of the worlds, one must look at it with the unbiased eyes of a child.
  * Political education in schools should be mandatory.
  
##  190103



  How to find the best partner?
  
  * 37 % rule:  the evaluate but reject the first 37 % of partners, 
                after that pick the next one that is better than the best of the first 37%
  * Don´t try to hide your differences because some might reject them.
    Show them so that you stick out. What some may reject, others might find deeply valuable.
    
  * If I increase the numbers of people I meet, I can find a more suitable partner.
  
##  190110

  Last Lecture: Surveillance
  
  * Mass surveillance is our problem because we don´t controll who has access to our data.
  * Even meta data (i.e. who called whom at what time, instead of the content of the phone conversation) is already very sensitive.
    A private detective would collect the same kind of meta data.
  * All our phones are equipped with a microphone, two cameras in opposing directions, a GPS receiver, a accelerometer and in many cases a constant internet connection.
    Mass surveillance as well as individual surveillance is very easy nowadays. All the sensors can be turned on without the owner noticing.
  * Mass collection of data in the U.S. has not prevented any terrorist attacks since 2003.
  * Chinas mass surveillance is just sad. People have no way to escape it and have real life consequences when they act in an "inapropriate" way.
  * No algorithm should ever rate if the amount of ice cream or other goods you buy is a bad or good choice. We are giving up our humanity and the freedom to do new things and make mistakes if we are
    constantly worried that an algorithm will rate us negatively. That way we will never improve and never try new things, we will just try to game the algorithm to gain the highest point.
